const displayBox = document.querySelector(".display");
/*-----------showDisplayFunc&DisplayBox---------*/
function showDisplayFunc(event){
    const x = event.target.innerText;
    let source ;
    // source = source.split("+");
    // console.log(source);
    // source = source.split("-");
    // console.log(source);
    if (displayFunc.innerText.length == 0){
        // source = x;
        displayFunc.innerText = x;
        displayBox.innerText =  x; 
    }
    else {
        // source += x;
        displayFunc.innerText += x;
        displayBox.innerText += x;
        // console.log(source)
    }
}

const list = document.querySelectorAll(".show-display");
list.forEach(item => {item.addEventListener("click", showDisplayFunc)});

/*-----------calculate---------*/
function cal (){
    displayFunc.innerText = "";   
    displayBox.innerText = eval( displayBox.innerText);
}
const calculate = document.querySelector(".calculate")
calculate.addEventListener("click", cal);

/*-----------allClear---------*/
function allClear (){  
    displayBox.innerText = 0;
    displayFunc.innerText='';
 }
document.querySelector(".all-clear").addEventListener("click", allClear);

/*-----------clearLast---------*/
function clearLast (){
    const text = displayBox.innerText;
    if(text.length === 1){
        displayBox.innerText = 0;
        displayFunc.innerText=' ';
    }
    else {
        displayBox.innerText = text.substring(0, text.length - 1);
        displayFunc.innerText=' ';
    }
 }
document.querySelector(".clear-last").addEventListener("click", clearLast);

/*-----------inverse---------*/
// const key = document.queryCommandValue(".key");
const displayFunc = document.querySelector(".displayFunc");
document.querySelector(".inverse").addEventListener("click", inverse);
function inverse (){
    let result = `1/${displayBox.innerText}`;
    displayFunc.innerText = result;
    displayBox.innerText = eval(result);
    // console.log(result)
}

/*-----------radical---------*/
document.querySelector(".radical").addEventListener("click", radical);
function radical (){
    let result = `√(${displayBox.innerText})`;
    displayFunc.innerText = result
    displayBox.innerText = Math.sqrt(`${displayBox.innerText}`) ;
    // cal ();
    console.log(result)
}

/*-----------Power2---------*/
document.querySelector(".Power2").addEventListener("click", Power2);
function Power2 (){
    let result = `sqr(${displayBox.innerText})`;
    displayFunc.innerText = result
    displayBox.innerText = Math.pow(`${displayBox.innerText}`, 2) ;
    // console.log(result)
}

